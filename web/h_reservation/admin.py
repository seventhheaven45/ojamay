from django.contrib import admin
from h_reservation.models import Product, Sales, SalesPerProduct, Store

admin.site.register(Product)
admin.site.register(Sales)
admin.site.register(SalesPerProduct)
admin.site.register(Store)
