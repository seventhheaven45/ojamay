import re

from datetime import datetime

from django import forms
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

from appointment.models import Appointment, Customer


class AppointmentForm(forms.Form):
    visit_date = forms.DateField(
        label="日付",
        input_formats=['%Y/%m/%d'],
        widget=forms.TextInput(
            attrs={
                'class': 'form-control datetimepicker-input',
                'data-target': '#date-picker',
                'placeholder': 'YYYY/mm/dd',
            }
        )
    )
    visit_time = forms.TimeField(
        label="時刻",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control datetimepicker-input',
                'data-target': '#time-picker',
                'placeholder': 'HH:MM',
            }
        )
    )

    def __init__(self, customer_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.customer_id = customer_id

    def clean(self):
        now = datetime.now()
        if Appointment.objects.filter(customer_id=self.customer_id, visit_at__gte=now).exists():
            self.add_error(None, '既に予約されています。')

        cleaned_data = self.cleaned_data
        visit_date = cleaned_data['visit_date']
        visit_time = cleaned_data['visit_time']
        visit_at = datetime.combine(visit_date, visit_time)
        if Appointment.objects.filter(customer_id=self.customer_id, visit_at__gte=visit_at).exists():
            self.add_error(None, '現在日時より前に予約しています。')

        return visit_at


class LoginForm(forms.Form):
    name = forms.CharField(label="お名前", max_length=50)
    phone_number = forms.CharField(label="電話番号", help_text="数字のみ；ハイフン不可", min_length=10, max_length=12)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone_number'].widget.attrs['aria-describedby'] = 'phoneNumberHelp'
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control mt-1'

    def clean(self):
        """
        is_valid() is True ---> Customerインスタンスを返す
        is_valid() is False ---> cleaned_dataを返す
        """
        cleaned_data = self.cleaned_data
        name = cleaned_data['name']
        phone_number = cleaned_data['phone_number']
        try:
            customer = Customer.objects.get(name=name, phone_number=phone_number)
            return customer.id
        except ObjectDoesNotExist:
            self.add_error('phone_number', 'お名前もしくは電話番号が間違っています。')
        except MultipleObjectsReturned:
            self.add_error('phone_number', 'ご入力いただいたユーザー情報が複数見つかりました。\nお手数ですが担当者にご連絡ください。')
        return cleaned_data
