from django.urls import path
from . import views

app_name = 'appointment'

urlpatterns = [
    path('', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('appointment/', views.appointment, name='appointment'),
]