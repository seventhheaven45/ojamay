import datetime

from django.contrib import messages
from django.http import HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.shortcuts import redirect, render

from appointment.forms import AppointmentForm, LoginForm
from appointment.models import Car, Customer, Appointment
from core.decorators import login_required, require_GET_POST


@require_GET_POST
def login(request):
    """ログイン"""
    if request.session.get('customer_id'):
        return redirect('appointment:appointment')
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            request.session["customer_id"] = form.cleaned_data
            return redirect('appointment:appointment')

    return render(request, 'appointment/login.html', {'form': form})


@login_required
@require_GET_POST
def logout(request):
    """ログアウト"""
    if request.session.get('customer_id'):
        del request.session["customer_id"]
    return redirect('appointment:login')


@login_required
@require_GET_POST
def appointment(request):
    """来店予約"""
    form = AppointmentForm(request.customer.id, request.POST or None)
    # form = 'form'
    if request.method == "POST":
        if form.is_valid():
            Appointment.objects.create(
                customer=request.customer,
                visit_at=form.cleaned_data,
            )
            return HttpResponse('OK')

    return render(request, 'appointment/appointment.html', {'form': form})
