$(function() {
  // 画面初期化用の関数
  function clearDisplay(){
    $('#display').empty();
    $('#multi_display').empty();
    $('#registered_products').empty();
    $('#total_price').text(0);
    total_price = 0;
    purchased_products = {};
    multi_register = null;
  }

  // 初期設定
  let total_price = 0;
  let purchased_products = {};
  let multi_register = 0;
  const text_product_not_exists = "商品が登録されていません。"
  const store_id = $('input[name="store_id"]').val();
  const products = $.parseJSON($('input[name="products"]').val());
  const products_cord = Object.keys(products);

  // 数字ボタン押下
  $('input.btn_num').on('click', function(){
    const num = $(this).val();
    if ($('#display').text() === text_product_not_exists) {
      $('#display').empty();
    }
    $('#display').append(num);
  });

  // Cボタン押下
  $('button#clear').on('click', function(){
    $('#display').empty();
    $('#multi_display').empty();
  });

  // 追加ボタン押下
  $('button#append').on('click', function(){
    const cord = $('#display').text();
    if ($.inArray(cord, products_cord) === -1) {
      $('#display').html('<span class="text-danger">' + text_product_not_exists + '</span>');
    }else{
      const product = products[cord];
      const registered_products = $('#registered_products').text();

      // 画面表示を変更
      $('#display').empty();
      $('#registered_products').append(product.name + "　¥"　+ product.price + "<br>");
      total_price = total_price + product.price
      $('#total_price').text(total_price);

      // DB登録処理用のオブジェクトを追加
      const purchased_products_cord = Object.keys(purchased_products);
      if ($.inArray(cord, purchased_products_cord) === -1) {
        purchased_products[cord] = 1;
      }else{
        purchased_products[cord] += 1;
      }
    }
  });

  // ×ボタン押下
  $('button#multi').on('click', function(){
    const display = $('#display').text();
    if ($.isNumeric(display)){
      multi_register = Number(display);
      $('#display').empty();
      $('#multi_display').text(multi_register);
    }
  });

  // 中止ボタン押下
  $('button#cancel').on('click', function(){
    clearDisplay();
  });

  // 確定ボタン押下
  $('button#confirm').on('click', function(){
    // csrfトークン
    if (Object.keys(purchased_products).length) {
      function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
            }
          }
        }
        return cookieValue;
      }

      var csrftoken = getCookie('csrftoken');

      function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }

      $.ajaxSetup({
        beforeSend: function (xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
        }
      });

      // pythonに購入データをajax送信
      $.ajax({
        'url': $('input[name="ajax_url"]').val(),
        'type': 'POST',
        'data': {
          'store_id': store_id,
          'products': JSON.stringify(purchased_products),
          'total_price': total_price
        },
        'dataType': 'json'
      });

      // 画面初期化
      clearDisplay();
    }
  });
});
