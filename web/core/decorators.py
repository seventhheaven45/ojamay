from functools import wraps

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods

from appointment.models import Customer


def login_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        customer_id = request.session.get('customer_id')
        if not customer_id:
            return redirect('appointment:login')
        try:
            customer = Customer.objects.get(id=customer_id)
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            del request.session['customer_id']
            return redirect('appointment:login')
        request.customer = customer
        return func(request, *args, **kwargs)
    return wrapper


require_GET_POST = require_http_methods(["GET", "POST"])
require_GET_POST.__doc__ = "Decorator to require that a view only accepts the GET and POST method."
